# build-env
Docker Build/CI Enviromnent for C++/VB.Net

Packages:
- GCC 8
- MinGW
- Make
- Mono
- NuGet
- Wine

Manually installed:
- CMake 3.17.1
- Boost 1.72.0 (Precompiled for GCC 8 and MinGW)
- NSIS 3.05
